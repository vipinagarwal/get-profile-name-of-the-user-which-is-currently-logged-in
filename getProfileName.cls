public without sharing class banAccountDetailController {
    @AuraEnabled
    public Static Boolean getProfileName(){
        /*
        * Method Name: getProfileName
        * Purpose : get profile name of Loggedin user.
        * Paramter : null
        * Return : profile name
        */  
        Id proId=UserInfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:proId].Name;
        if(profileName=='Neighbor Administrator'){
            return true;
        }
         return false; 
    }
}